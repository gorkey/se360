package myPackage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

import myPackage.Game.Player;
import myPackage.Server;



public class Server {
	
	public static LinkedList<Player> clients = new LinkedList<Player>();
	
	
	
	public static void main(String[] args) throws Exception {
        ServerSocket listener = new ServerSocket(22222);
        System.out.println("Server is Running");
        
        try {
            while (true) {
                Game game = new Game();
                Game.Player player1 = game.new Player(listener.accept(), '1');
                Game.Player player2 = game.new Player(listener.accept(), '2');
                
                player1.setOpponent(player2);
                player2.setOpponent(player1);
                
                player1.start();
                player2.start();
              
                
                
                
            }
        } finally {
            listener.close();
            
        }
    }
}
class Game {
	
	
	
	
	
	
	public int player1Move = 0;
	public int player2Move = 0;
	
	public int player1Score = 0;
	public int player2Score = 0;
	
	int temp1 = 0;
	int temp2 = 0;
	
	String score;
	
	 public boolean RoundSystem()
     {
     	if(player1Move!=0&&player2Move!=0)
     	{
     		if(player1Move==player2Move)
     		{
     			score = "tie";
     		}
     		else if(player1Move == 1 && player2Move == 3)
     		{
     			player1Score++;
     			score = "player1scores";
     		}
     		else if(player1Move == 2 && player2Move == 1)
     		{
     			player1Score++;
     			score = "player1scores";
     		}
     		else if(player1Move == 3 && player2Move == 2)
     		{
     			player1Score++;
     			score = "player1scores";
     		}
     		else if(player2Move == 1 && player1Move == 3)
     		{
     			player2Score++;
     			score = "player2scores";
     		}
     		else if(player2Move == 2 && player1Move == 1)
     		{
     			player2Score++;
     			score = "player2scores";
     		}
     		else if(player2Move == 3 && player1Move == 2)
     		{
     			player2Score++;
     			score = "player2scores";
     		}
     		
     		
     		System.out.println("player1 " + player1Score + " player2 " + player2Score);
     		return true;
     		
     	
     	}
     	
     	return false;
     }
	 
	 void ResetRound()
	 {
		 player1Move = 0;
		 player2Move = 0;
		 
		 
		 
	 }
	 
	 
	class Player extends Thread {
        char mark;
        Player opponent;
        Socket socket;
        BufferedReader input;
        PrintWriter output;
        // thread handler to initialize stream fields
        public Player(Socket socket, char mark) {
            this.socket = socket;
            this.mark = mark;
            if(Server.clients.size() >= 2)
            {
            	Server.clients.clear();
            }
            if(mark=='1')
            {
            	Server.clients.addFirst(this);
            }
            else if(mark=='2')
            {
            	Server.clients.addLast(this);
            }
            
            
            
            for(int z = 0 ; z < Server.clients.size() ; z++)
            {
            	System.out.println("z: " + Server.clients.get(z));
            }
            
            System.out.println("mark " + mark);
            try {
                input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
                output = new PrintWriter(socket.getOutputStream(), true);
                output.println("WELCOME " + mark);
                output.println("MESSAGE Waiting for opponent to connect");
            } catch (IOException e) {
                System.out.println("Player died: " + e);
                
                
            }
        }
        
        
        
       
        
        //Accepts notification of who the opponent is.
        public void setOpponent(Player opponent) {
            this.opponent = opponent;
        }
	
        public void run() {
        	
        	
            try {
                // The thread is only started after everyone connects.
                output.println("MESSAGE All players connected");

             
                
                

                // Repeatedly get commands from the client and process them.
                while (true) {
                	
                    String command = input.readLine();
                    //System.out.println(command);
                    if (command.startsWith("11")) {
                    	System.out.println("1 Rock");
                    	player1Move = 1;
                    	temp1 = 1;
                    }
                    else if (command.startsWith("21")) {
                    	System.out.println("2 Rock");
                    	player2Move = 1;
                    	temp2 = 1;
                    }
                    else if (command.startsWith("12")) {
                    	System.out.println("1 Paper");
                    	player1Move = 2;
                    	temp1 = 2;
                    }
                    else if (command.startsWith("22")) {
                    	System.out.println("2 Paper");
                    	player2Move = 2;
                    	temp2 = 2;
                    }
                    else if (command.startsWith("13")) {
                    	System.out.println("1 Scissors");
                    	player1Move = 3;
                    	temp1 = 3;
                    }
                    else if (command.startsWith("23")) {
                    	System.out.println("2 Scissors");
                    	player2Move = 3;
                    	temp2 = 3;
                    }
                    
                    
                    
                    if(RoundSystem())
                    {
                    	
                    	if(score=="player1scores")
                		{
                			Server.clients.get(0).output.println("WINROUND");
                			Server.clients.get(1).output.println("LOSEROUND");
                		}
                		else if(score=="player2scores")
                		{
                			Server.clients.get(1).output.println("WINROUND");
                			Server.clients.get(0).output.println("LOSEROUND");
                		}
                		else if(score=="tie")
                		{
                			Server.clients.get(0).output.println("TIEROUND");
                			Server.clients.get(1).output.println("TIEROUND");
                		}
                    	
                    	for(int i = 0 ; i < Server.clients.size() ; i++ )
                    	{
                    		
                    		
                    		if (i == 0) {
                    			Server.clients.get(i).output.println("2"+temp2);
                    		}
                    		else if(i==1)
                        	{
                        		Server.clients.get(i).output.println("1"+temp1);
                        	}
                    		
                    	}
                    	
                    	try
                    	{
                    	    Thread.sleep(3000);
                    	}
                    	catch(InterruptedException ex)
                    	{
                    	    Thread.currentThread().interrupt();
                    	}
                    	
                    	for(int i = 0 ; i < Server.clients.size() ; i++ )
                    	{
                    		
                    		if (i == 0) {
                    			Server.clients.get(i).output.println("reset");
                    		}
                    		else if(i==1)
                        	{
                        		Server.clients.get(i).output.println("reset");
                        	}
                    		
                    		if(player1Score == 3 ||  player2Score == 3 )
                    		{
                    			Server.clients.get(i).output.println("ENDMATCH");
                    		}
                    		
                    	}
                    	
                    	
                    	ResetRound();
                    	
                    }
                    
                    if (command.startsWith("QUIT")) {
                    	
                    	
                        return;
                    }
                }
            } catch (IOException e) {
                System.out.println("Player died: " + e);
                
                
            } finally {
                try 
                {
                	
                	
                	
                socket.close();
                
                } 
                catch (IOException e) {}
            }
        }
        
        
        
        
        
        
	}
	
	
	
	
}
