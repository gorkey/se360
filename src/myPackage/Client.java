package myPackage;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;
public class Client implements ActionListener{

	boolean clicked = false;
	String sendTemp;
	
	JButton buttonr = new JButton(" ");
	JButton buttonp = new JButton("P");
	JButton buttons = new JButton("S");
	
	Image rock = null;
	Image paper = null;
	Image scissors = null;
	Image rocko = null;
	Image papero = null;
	Image scissorso = null;
	
	JFrame jf = new JFrame();
	JPanel jp = new JPanel();
	JLabel jlp = new JLabel();
	JLabel jlo = new JLabel();
	JLabel jllog = new JLabel("",SwingConstants.CENTER);
	JLabel jlscore = new JLabel("",SwingConstants.CENTER);
	JLabel jlcounter = new JLabel("",SwingConstants.CENTER);
	private JLabel messageLabel = new JLabel("");
	
	int scoreforplayer = 0;
	int scoreforopponent = 0;
	String whoWin;
	
	
	private static int PORT;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    
    
    char mark;
	public static void main(String[] args) throws Exception {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter Server IP: ");
        String serverAddress = scan.nextLine();
        System.out.println("Enter Port ");
        PORT = scan.nextInt();
		
        
		while (true) {
			
            Client client = new Client(serverAddress , PORT);
            client.jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            
            client.jf.setVisible(true);
            client.play();
            if (!client.wantsToPlayAgain()) {
                break;
            }
            
        }

	}
	
	
	
	public Client(String serverAddress , int PORT) throws Exception
	{
		
		 // Setup networking
        socket = new Socket(serverAddress, PORT);
        in = new BufferedReader(new InputStreamReader(
        socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);

		
		jlp.setBounds(400, 400, 320, 320);
		jlo.setBounds(800, 400, 320, 320);
		jllog.setBounds(600, 200, 500, 320);
		jlscore.setBounds(600, 0 , 500, 320);
		jlcounter.setBounds(600, 600 , 500, 320);
		jf.setSize(1700, 1080);
		jf.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		jf.setVisible(true);
		
		
		jp.setLayout(null);
		
		
		
		
		
		
		
		
		buttonr.setBounds(50, 30, 320, 300);
		
		
		buttonp.setBounds(50, 350, 320, 300);
		
		
		buttons.setBounds(50,670 , 320, 300);
		
		buttonr.addActionListener(this);		     
	    buttonp.addActionListener(this);
	    buttons.addActionListener(this);
		
		
		
		
		try {
		rock = ImageIO.read(getClass().getResource("/Imagez/Images/rock.png"));
		paper = ImageIO.read(getClass().getResource("/Imagez/Images/paper.png"));
		scissors = ImageIO.read(getClass().getResource("/Imagez/Images/scissors.png"));
		rocko = ImageIO.read(getClass().getResource("/Imagez/Images/rock2.png"));
		papero = ImageIO.read(getClass().getResource("/Imagez/Images/paper2.png"));
		scissorso = ImageIO.read(getClass().getResource("/Imagez/Images/scissors2.png"));
		
		
		
		buttonr.setIcon(new ImageIcon(rock));
		buttonp.setIcon(new ImageIcon(paper));
		buttons.setIcon(new ImageIcon(scissors));
		}
		catch (Exception ex) {
		    System.out.println(ex);
		    
		  }
		
		
		
		jp.add(buttonr);
		jp.add(buttonp);
		jp.add(buttons);
		
		messageLabel.setBackground(Color.lightGray);
        jf.getContentPane().add(messageLabel, "South");
        
        jllog.setFont(new Font("Serif" , Font.BOLD , 40));
        jlscore.setFont(new Font("Serif" , Font.BOLD , 40));
        jlcounter.setFont(new Font("Serif" , Font.BOLD , 40));
        
		jp.add(jlp);
		jp.add(jlo);
		jp.add(jllog);
		jp.add(jlscore);
		jp.add(jlcounter);
		jf.add(jp);
		
	}
	
	public void play() throws Exception {
		int i = 0;
	
        String response;
        try {
            response = in.readLine();
            if (response.startsWith("WELCOME")) {
                mark = response.charAt(8);
                
                jf.setTitle("Rock Paper Scissors - Player " + mark);
            }
            while (true) {
            	boolean hasStarted = false;
                response = in.readLine();
                System.out.println(response);
                
                jlscore.setText("YOU: " + scoreforplayer + " - " + scoreforopponent + ": OPPONENT");
                
                if(response.startsWith("MESSAGE All players connected"))
            	{
                	jlo.setIcon(null);
            		jlp.setIcon(null);
            		hasStarted = true;
            	}
                
                if(hasStarted)
                {
                	CounternRandom();
                }
                
                
                
                	if(response.startsWith("21"))
                	{
                		jlo.setIcon(new ImageIcon(rocko));
                	}
                	else if(response.startsWith("22"))
                	{
                		jlo.setIcon(new ImageIcon(papero));
                	}
                	else if(response.startsWith("23"))
                	{
                		jlo.setIcon(new ImageIcon(scissorso));
                	}
                	
                	
                    
                
                
                	
                	if(response.startsWith("11"))
                	{
                		jlo.setIcon(new ImageIcon(rocko));
                	}
                	else if(response.startsWith("12"))
                	{
                		jlo.setIcon(new ImageIcon(papero));
                	}
                	else if(response.startsWith("13"))
                	{
                		jlo.setIcon(new ImageIcon(scissorso));
                	}
                	
                	if(response.startsWith("WINROUND"))
                	{
                		jllog.setText("YOU WIN THIS ROUND");;
                		scoreforplayer++;
                	}
                	else if(response.startsWith("LOSEROUND"))
                	{
                		jllog.setText("YOU LOSE THIS ROUND");;
                		scoreforopponent++;
                	}
                	else if(response.startsWith("TIE"))
                	{
                		jllog.setText("TIE!");;
                	}
                	
                	if(response.startsWith("reset"))
                	{
                		clicked = false;
                		jlo.setIcon(null);
                		jlp.setIcon(null);
                		jllog.setText(null);
                		jlcounter.setText(null);
                		
                		System.out.println(++i);
                		
                		CounternRandom();
                	}
                    
                
                
                if (response.startsWith("ENDMATCH")) {
                	if(scoreforplayer==3)
                	{
                		whoWin = "You Win! ";
                	}
                	else if(scoreforopponent==3)
                	{
                		whoWin = "You Lose! ";
                	}
                   
                    break;
                }
            }
            out.println("QUIT");
        }
        finally {
        	
            socket.close();
        }
    }
	
	private void CounternRandom()
	{
		Random rand = new Random();
		
		for (int z = 0 ; z <3 ; z++)
        {
			if(scoreforplayer == 3 || scoreforopponent == 3)
			{
				return;
			}
			
			if(z == 0)
        	{
        		jlcounter.setText("Ready?");
        	}
			
			try
        	{
        	    Thread.sleep(1000);
        	}
        	catch(InterruptedException ex)
        	{
        	    Thread.currentThread().interrupt();
        	}
			
        	if(z == 0)
        	{
        		jlcounter.setText("Rock");
        	}
        	else if(z == 1)
        	{
        		jlcounter.setText("Paper");
        	}
        	else if(z == 2)
        	{
        		jlcounter.setText("Scissors");
        	}
        	
        	
        	
        	if(z==2 && (clicked == false))
        	{
        		int i = rand.nextInt(3);
        		
        		if(i == 0)
        		{
        			jlp.setIcon(new ImageIcon(rock));
       	    	 	out.println(mark+"1");
        		}
        		else if(i == 1)
        		{
        			jlp.setIcon(new ImageIcon(paper));
       	    	 	out.println(mark+"2");
        		}
        		else if(i == 2)
        		{
        			jlp.setIcon(new ImageIcon(scissors));
       	    	 	out.println(mark+"3");
        		}
        	}
        	else if(z==2 && (clicked == true))
        	{
        		out.println(sendTemp);
        	}
        }
	}
	
	private boolean wantsToPlayAgain() {
        int response = JOptionPane.showConfirmDialog(jf,"Want to play again?",whoWin + "Score: " + scoreforplayer + "-" + scoreforopponent,JOptionPane.YES_NO_OPTION);
        jf.dispose();
        return response == JOptionPane.YES_OPTION;
    }

	public void actionPerformed(ActionEvent e)
	  {
	     String str = e.getActionCommand();   
	     System.out.println(str);
	     if(str == " ")
	     {
	    	 jlp.setIcon(new ImageIcon(rock));
	    	 sendTemp = mark+"1";
	    	 clicked = true;
	     }
	     else if(str== "P")
	     {
	    	 jlp.setIcon(new ImageIcon(paper));
	    	 sendTemp = mark+"2";
	    	 clicked = true;
	     }
	     else if(str== "S")
	     {
	    	 jlp.setIcon(new ImageIcon(scissors));
	    	 sendTemp = mark+"3";
	    	 clicked = true;
	     }
	     
	  }

}